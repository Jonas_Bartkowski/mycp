#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>


static bool d1 = true;
static bool d2 = true;

int main(int argc, char* argv[])
{
    if (argc == 3)
    {
        char* opath = argv[1]; //Q: do i need to call it spath and tpath?
        char* dpath = argv[2];

        int fpo = open(opath, O_WRONLY | O_APPEND);
        int fpd = open(dpath, O_WRONLY | O_APPEND);

        if(fpo < 0)
            fprintf(stderr, "Error opening file '%s'!\n", opath);

        if(fpd < 0)
            fprintf(stderr, "Error opening file '%s'!\n", dpath);

        off_t offset = lseek(fpo, 0, SEEK_CUR); //Q: in the internet I found people casting 0 to ize_t, why?
        long size_ofile = lseek(fpo, offset, SEEK_END);

        if (d1) printf("Read %li bytes!\n", size_ofile);
        //compare if files are the same hardlink and abort if it's the case

        char* buf = malloc(1 * (size_t) size_ofile);
        read(fpo, buf, (size_t) size_ofile);//Q: how big does the buffer need to be?


        write(fpd, buf, (size_t) size_ofile);


        //use cpy to create copy of opath at dpath
            //if possible set mode of open, to mode of opath (modified by the current umask).
                //Q: what does this mean?
            //Q: create intermediate directories if missing?
            //if dpath exists already, overwrite
                //except if opath is same file/hardlink as dpath, abort then and don't change mode
            //instead of only 0's, create holes in file

        free(buf);
    }
    else
    {
        fprintf(stderr, "Please exactly supply an origin path and a destination path!\n");
        return -1;
    }
}
