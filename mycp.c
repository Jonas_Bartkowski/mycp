#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


static bool d1 = false;

bool isSameFile(struct stat o_stat, struct stat d_stat)
{
    return (o_stat.st_dev == d_stat.st_dev && o_stat.st_ino == d_stat.st_ino);
}

int main(int argc, char* argv[])
{
    if (argc == 3)
    {
        char* opath = argv[1]; //Q: do i need to call it spath and tpath?
        char* dpath = argv[2];

        int fpo = open(opath, O_RDONLY);

        if(fpo < 0)
        {
            fprintf(stderr, "Error opening origin file '%s'!\n", opath);
            return -1;
        }

        struct stat o_stat;
        if (fstat(fpo, &o_stat) < 0)
        {
            fprintf(stderr, "Error reading origin file data!\n");
            return -2;
        }
        if (d1) printf("Read file-pointer %d for origin file!\n", fpo);
        if (d1) printf("o_stat has %ld blocks!\n", o_stat.st_blocks);

        int fpd = open(dpath, O_WRONLY | O_TRUNC, o_stat.st_mode);
        struct stat d_stat;
        if(fpd < 0)
        {
            if (stat(dpath, &d_stat))
            {
                // file exists
                fprintf(stderr, "Error opening destination file '%s'!\n", dpath);
                return -3;
            }
            else
            {
                fpd = open(dpath, O_CREAT | O_WRONLY, o_stat.st_mode);
                if (d1) printf("Opening destination file in create mode!\n");
                if (fpd < 0)
                {
                    fprintf(stderr, "Error opening destination file '%s'!\n", dpath);
                    return -4;
                }
            }
        }
        else if (fstat(fpd, &d_stat) < 0)
        {
            fprintf(stderr, "Error reading origin file data!\n");
            return -5;
        }

        //compare if files are the same hardlink and abort if it's the case
        if (isSameFile(o_stat, d_stat))
        {
            fprintf(stderr, "Please do not supply the same file as origin and destination path!\n");
            return -6;
        }

        if (d1) printf("Read file-pointer %d for destination file!\n", fpd);

        lseek(fpo, 0, SEEK_SET);
        lseek(fpd, 0, SEEK_SET);

        //The following is my code to read and write block by block.
        //I tried a lot, but for example, o_stat.st_blocks always returns 2x the amount
        //of blocks that my ls -ls command gives me. Even if I divide it by 2 manually,
        //the file ends up being way bigger than the origin file. It just wouldn't work.
        //See my workaround after this commented-out section.

        /*

        size_t block_size = (size_t) o_stat.st_blksize;
        ssize_t blocks_to_write = o_stat.st_blocks / 2;
        char* sparse_block = calloc(block_size, 1);
        char* buf = malloc(block_size);

        if (d1) printf("Reading %ld blocks a %ld bytes from origin file!\n", blocks_to_write, o_stat.st_blksize);

        long block_num = 0;
        while (block_num < blocks_to_write-1)
        {
            read(fpo, buf, block_size);
            if (!memcmp(buf, sparse_block, block_size))
            {
                write(fpd, buf, block_size);
            }
            else
            {
                if (d1) printf("0'd block detected on block %ld. Skipping!\n", block_num+1);
                lseek(fpd, (off_t) block_size, SEEK_CUR);
            }
            block_num++;
        }
        read(fpo, buf, block_size);
        write(fpd, buf, block_size);

        */

        //This is the best solution I came up with.
        //still reads & writes block by block, but without
        //using st_blocks.
        //I tried using memcmp, but the file doesn't
        //end up correctly if I lseek instead of write, although it
        //will end up being of the same size.


        ssize_t bsize = o_stat.st_blksize;
        char* buf = malloc((size_t) bsize);
        //char* sparse_block = calloc(bsize, 1);
        off_t written = 0;
        while (written + bsize < o_stat.st_size)
        {

            read(fpo, buf, (size_t) bsize);
            /*
            if (!memcmp(buf, sparse_block, bsize))
            {
                write(fpd, buf, bsize);
            }
            else
            {
                lseek(fpd, (off_t) bsize, SEEK_CUR);
            }
            */
            write(fpd, buf, (size_t) bsize);
            written += bsize;
        }
        read(fpo, buf, o_stat.st_size-written);
        write(fpd, buf, o_stat.st_size-written);

        if (d1) printf("Freeing buffer...\n");
        free(buf);
        //free(sparse_block);
        if (d1) printf("Done!\n");
        return 0;
    }
    else
    {
        fprintf(stderr, "Please exactly supply an origin path and a destination path!\n");
        return -1;
    }
}
